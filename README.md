Following list shows Avestan corresponding English letters and Unicode characters which could be used to write Sara Pârsig (refined Persian). Also, north Indian notation systems have been adopted for digits. Digits and punctuation marks have been flipped horizontally, as Avestan is written from right to left.


a: a<br/>
b: b<br/>
c: c<br/>
d: d<br/>
e: e<br/>
f: f<br/>
g: g<br/>
h: h<br/>
i: i<br/>
j: j<br/>
k: k<br/>
l: l<br/>
m: m<br/>
n: n<br/>
o: o<br/>
p: p<br/>
q: q (gh)<br/>
r: r<br/>
s: s<br/>
t: t<br/>
u: u<br/>
v: v<br/>
x: x<br/>
y: y<br/>
z: z<br/>
A: â (ā)<br/> 
B: bv<br/>
C: ŝc (shc)<br/>
E: ē<br/>
F: ŝf (shf)<br/>
H: ŝh (shh)<br/>
I: ī<br/>
J: ŝj (shj)<br/>
N: ny (alternate form)<br/>
O: ō<br/>
S: ŝ (sh)<br/>
T: tt<br/>
U: ū<br/>
V: v (alternate form)<br/>
X: ŝx (shx)<br/>
Y: y (alternate form)<br/>
Z: ž (zh)<br/>
0: 0<br/>
1: 1<br/>
2: 2<br/>
3: 3<br/>
4: 4<br/>
5: 5<br/>
6: 6<br/>
7: 7<br/>
8: 8<br/>
9: 9<br/>
00C5 + Alt + x: Å<br/>
00C6 + Alt + x: aee<br/>
00E3 + Alt + x: an<br/>
00E5 + Alt + x: å<br/>
00E6 + Alt + x: ae<br/>
00F1 + Alt + x: ngv<br/>
0105 + Alt + x: aen<br/>
0121 + Alt + x: gg<br/>
0144 + Alt + x: ny<br/>
014A + Alt + x: ngy<br/>
014B + Alt + x: ng<br/>
015B + Alt + x: shy<br/>
03B2 + Alt + x: bh<br/>
03B4 + Alt + x: dh<br/>
03B8 + Alt + x: th<br/>
1E25 + Alt + x: xy<br/>
1E43 + Alt + x: hm<br/>
1E47 + Alt + x: nn<br/>
1E63 + Alt + x: ssh<br/>
1E8B + Alt + x: xv<br/>
1E8F + Alt + x: yy<br/>


For example, following is how to reverse the order of English letters from right to left and write the first verses from Šâhnâmag (The Book of Kings):

darx du nAj e dnavAdox e mAn dap<br/>
darazogen rab ratrab aSidna ni za ek<br/>
nAj e dnavAdox du mAn e dnavAdox<br/>
yAmenhAr e hed izur e dnavAdox<br/>
